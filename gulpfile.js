var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
	uglify = require('gulp-uglify'),
	notify = require("gulp-notify"),
	watch = require('gulp-watch'),
	rename = require("gulp-rename"),
	livereload = require('gulp-livereload'),
	connect = require('gulp-connect'),
	sass = require('gulp-sass'),
	jshint = require('gulp-jshint'),
	fileinclude = require('gulp-file-include'),
	concat = require('gulp-concat');

var path = {
	build: {
		html: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/img/',
		fonts: 'build/fonts/',
		libs: 'build/libs/'
	},
	src: {
		html: 'src/*.*',
		js: 'src/js/*.js',
		style: 'src/style/*.scss',
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*',
		libs: 'src/libs/**/*.*'
	},
	watch: {
		html: 'src/**/*.html',
		js: 'src/js/**/*.js',
		style: 'src/style/**/*.scss',
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*',
		libs: 'src/libs/**/*.*'
	}
};

gulp.task('lint', function () {
	return gulp.src('src/js/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('connect', function () {
	return connect.server({
		root: 'build',
		port: 8888,
		livereload: true
	});
});


gulp.task('html:build', function () {
	return gulp.src(path.src.html)
		.pipe(plumber())
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe(gulp.dest(path.build.html))
		.pipe(connect.reload());
		//.pipe(notify("html ready!"));
});

gulp.task('js:build', function () {
	return gulp.src(path.src.js)
		.pipe(sourcemaps.init())
		.pipe(plumber())
		.pipe(concat('main.js'))
		.pipe(sourcemaps.write()) 
		.pipe(gulp.dest(path.build.js))
		.pipe(connect.reload());
		//.pipe(notify("js ready!"));
});

gulp.task('style:build', function () {
	return gulp.src(path.src.style)
		.pipe(sourcemaps.init())
		.pipe(plumber())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.css))
		.pipe(connect.reload());
		//.pipe(notify("css ready!"));
});

gulp.task('image:build', function () {
	return gulp.src(path.src.img)
		.pipe(gulp.dest(path.build.img));
});

gulp.task('fonts:build', function () {
	return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts));
});
gulp.task('libs:build', function () {
	return gulp.src(path.src.libs)
		.pipe(gulp.dest(path.build.libs));
});



gulp.task('build', [
	'html:build',
	'js:build',
	'style:build',
	'fonts:build',
	'image:build',
	'libs:build'
]);

gulp.task('watch', function () {

	watch([path.watch.html], function (event, cb) {
		gulp.start('html:build');
	});
	watch([path.watch.style], function (event, cb) {
		gulp.start('style:build');
	});
	watch([path.watch.js], function (event, cb) {
		gulp.start('js:build');
	});
	/*
	watch([path.watch.img], function (event, cb) {
		gulp.start('image:build');
	});
	watch([path.watch.fonts], function (event, cb) {
		gulp.start('fonts:build');
	});
	watch([path.watch.libs], function (event, cb) {
		gulp.start('libs:build');
	});
	*/
});

gulp.task('default', ['build', 'connect', 'watch', 'lint']);