(function ($) {
	// html template
	/*	<div class="js-tabs tabs">
	 <ul class="tabs_nav">
	 <li><a href="#test"></a></li>
	 <li><a href="#test2"></a></li>
	 </ul>
	 <div id="test" class="tabs_content"></div>
	 <div id="test2" class="tabs_content"></div>
	 </div>*/
	// prived methods

	var initTabs = function (settings) {
		// For each set of tabs, we want to keep track of
		// which tab is active and its associated content
		var $tabs = this,
				$active,
				$content,
				$links = $tabs.find('.tabs_nav li a');

		// If the location.hash matches one of the links, use that as the active tab.
		// If no match is found, use the first link as the initial active tab.
		$active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
		$active.addClass(settings.activeClass);

		$content = $tabs.find($active[0].hash);

		// Hide the remaining content
		$links.not($active).each(function () {
			$tabs.find(this.hash).hide();
		});

		// Bind the click event handler
		$links.off('click'); // remove other listeners if exist
		$links.data("ignore-scroll", true);
		$links.on('click', function (e) {
			// Make the old tab inactive.
			$active.removeClass(settings.activeClass);
			$content.hide();

			// Update the variables with the new link and content
			$active = $(this);
			$content = $tabs.find(this.hash);

			// Make the tab active.
			$active.addClass(settings.activeClass);
			$content.show();

			// onTabs callback
			if (settings.onTabs) {
				settings.onTabs.call($tabs);
			}

			// Prevent the anchor's default click action
			e.preventDefault();

		});

	};
	// prived methods end


	// public methods
	var methods = {
		init: function (options) {

			// extend default option
			var settings = $.extend({}, $.fn.simpleTabs.defaults, options);

			return this.each(function () {

				var $tabs = $(this);
				var data = $tabs.data("simple-tabs");

				// if plugin was init
				if (!data) {
					// init plugin
					initTabs.call($tabs, settings);
					// set callback
					if (settings.onInit) {
						settings.onInit.call($tabs);
					}
					$tabs.data("simple-tabs", "init");
				}
			});
		},
		destroy: function () {

			// extend default option


			return this.each(function () {
				var defaultOpt = $.fn.simpleTabs.defaults;
				var $tabs = $(this),
						$links = $tabs.find('.tabs_nav li a');
						
				$tabs.removeData("simple-tabs");
				$links.removeClass(defaultOpt.activeClass);
				$tabs.find("> div").show();
				$links.off('click');
			});
		}
	};
	// public methods end
	$.fn.simpleTabs = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' not exist');
		}
	};

	// Plugin defaults – added as a property on our plugin function.
	$.fn.simpleTabs.defaults = {
		activeClass: "active",
		onInit: function () {},
		onTabs: function () {}
	};

})(jQuery);

// Global module
var Tabs = {
	init: function (selector) {
		$(selector).simpleTabs({
			activeClass: "is-current",
			onInit: function () {
				//console.log("tabs init");
			},
			onTabs: function () {
				//console.log("tabs change");
			}
		});
	},
	destroy: function (selector) {
		$(selector).simpleTabs('destroy');
	}
};

$(function () {
	Tabs.init(".js-tabs");
}); //ready