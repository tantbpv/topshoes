// alert.js
// validation and alert pop-ups
$(function () {
	initAlert();
}); // ready
// alert functions
var alertPopUp = function (id, notСlose) {
	var showPopUp = setTimeout(function () {
		$.magnificPopup.open({
			items: {
				src: id
			},
			callbacks: {
				open: isOpenPopup,
				close: isClosePopup,
				afterClose: function () {
					clearTimeout(hidePopUp);
					//console.log("clearTimeout");
				}
			},
			type: 'inline',
			preloader: false,
			removalDelay: 400,
			mainClass: 'mfp-fade',
			showCloseBtn: false,
			overflowY: 'scroll'
		});
	}, 500);

	if (!notСlose) {
		var hidePopUp = setTimeout(function () {
			$.magnificPopup.close();
		}, 5000);
	}
	//console.log($.magnificPopup.instance);
};

var openFVCallback = function() {
	$.magnificPopup.close();
	setTimeout(function () {
		$.magnificPopup.open({
			items: {
				src: "#callback"
			},
			type: 'inline',
			preloader: false,
			removalDelay: 400,
			mainClass: 'mfp-fade',
			showCloseBtn: false,
			overflowY: 'scroll',
			callbacks: {
				open: isOpenPopup,
				close: isClosePopup
			}
		});
	}, 500);
};

var checkSize = function () {
	var size = $(".js-prod-size");
	if (size.find("input[name=product-size]:checked").val()) {
		return size.find("input[name=product-size]:checked").val();
	} else {
		return false;
	}
	console.log("check size");
};

var toCart = function (size) {
	console.log("Товар с размером " + size + " добавлен в корзину!");
	//$.magnificPopup.close();
	//alertPopUp('#in-cart');
};

var chooseSize = function () {
	var size = $(".js-prod-size-popup");
	size.find("input[name=product-size]").on("change", function () {
		toCartComplete();
		//toCart($(this).val());
		//$.magnificPopup.close();
		//alertPopUp('#in-cart');
		//console.log($(this).val());
	});
};

// alert pop-ups actions
var checkoutComplete = function () {
	$.magnificPopup.close();
	alertPopUp("#checkout-complete");

	setTimeout(function () {
		ratingPopup();
	}, 8000);
};

var subscribeComplete = function () {
	//$.magnificPopup.close();
	alertPopUp("#subscribe-complete");
};

var callbackComplete = function () {
	$.magnificPopup.close();
	alertPopUp("#callback-complete");

	setTimeout(function () {
		ratingPopup();
	}, 8000);

};

var toCartComplete = function () {
	$.magnificPopup.close();
	alertPopUp("#in-cart", true);
};

var ratingPopup = function () {
	$.magnificPopup.close();
	alertPopUp("#rating", true);
};

function initAlert() {
	// listeners and actions

	var fvCallbackBtn = $(".js-fv-callback-pop-up");
	fvCallbackBtn.on("click", openFVCallback);
		
		
		
	var toCartBtn = $(".js-to-cart");
	toCartBtn.on("click", function () {
		//console.log("js-to-cart click");
		if (checkSize()) {
			//toCart(checkSize());
			toCartComplete();
		} else {
			$.magnificPopup.close();
			setTimeout(function () {
				$.magnificPopup.open({
					items: {
						src: '#check-size'
					},
					overflowY: 'scroll',
					callbacks: {
						open: function () {
							isOpenPopup();
							chooseSize();
						},
					 	close: isClosePopup
					},
					type: 'inline',
					preloader: false,
					removalDelay: 400,
					mainClass: 'mfp-fade',
					showCloseBtn: false
				});
			}, 500);
		}
		return false;
	});
	
	var checkoutForm = $(".js-checkout-form");
	checkoutForm.on("submit", function () {
		var $form = $(this);
		//console.log( "data: " + $form.serialize() );
		// sending form via ajax
		$.ajax({
			url: $form.attr('action') + "?ajax=true",
			type: $form.attr('method') || "POST",
			data: $form.serialize(),
			success: checkoutComplete,
			//error: checkoutComplete,
			error: function () {
				alert('Возникла ошибка, нет связи с сервером. Попробуйте еще раз.');
				console.log('error!');
			}
		});
		return false;
	});

	var subscribeform = $(".js-subscribe-form");
	subscribeform.on("submit", function () {
		var $form = $(this);
		//console.log( "data: " + $form.serialize() );
		// sending form via ajax
		$.ajax({
			url: $form.attr('action') + "?ajax=true",
			type: $form.attr('method') || "POST",
			data: $form.serialize(),
			success: subscribeComplete,
			//error: subscribeComplete,
			error: function () {
				alert('Возникла ошибка, нет связи с сервером. Попробуйте еще раз.');
				console.log('error!');
			}
		});
		return false;
	});

	var callbackform = $(".js-callback-form");
	callbackform.on("submit", function () {
		var $form = $(this);
		//console.log( "data: " + $form.serialize() );
		// sending form via ajax
		$.ajax({
			url: $form.attr('action') + "?ajax=true",
			type: $form.attr('method') || "POST",
			data: $form.serialize(),
			success: callbackComplete,
			//error: callbackComplete,
			error: function () {
				alert('Возникла ошибка, нет связи с сервером. Попробуйте еще раз.');
				console.log('error!');
			}
		});

		return false;
	});
	// listeners and actions end
}

// validation and alert pop-ups end
// alert.js end