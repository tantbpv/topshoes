$(document).ready(function () {
	var prodGalleryOptions = {
		slidesToShow: 7,
		slidesToScroll: 1,
		arrows: false,
		infinite: true,
		swipeToSlide: true,
		autoplay: true,
		autoplaySpeed: 2000,
		centerPadding: '6.25%',
		centerMode: true,
		accessibility: false,
		responsive: [
			{
				breakpoint: 1500,
				settings: {
					slidesToShow: 5,
					centerPadding: '8%'
				}
			},
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
					centerPadding: '12.5%'
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					centerPadding: '25%'
				}
			}
		]
	};
	var prodGallery = $('.js-prod-gallery');

	function initProductGallery(gallery, options) {
		gallery.slick(options);
		var slickObj = gallery.slick('getSlick');
		// is grid or slider
		toggleGrid(gallery);
	}

	function isGrid(gallery) {
		var $productGallery = gallery;
		var slick = $productGallery.slick('getSlick');
		var slidesCount = slick.$slides.length;
		var slidesToShow = slick.options.slidesToShow;
		return (slidesCount <= slidesToShow);
	}

	function toggleGrid(gallery) {
		if (isGrid(gallery)) {
			gallery.addClass("is-grid");
		} else {
			gallery.removeClass("is-grid");
		}
	}

	// init gallery
	prodGallery.each(function () {
		initProductGallery($(this), prodGalleryOptions);
	});

	$(window).resize(function () {
		// toggle grid class depend of screen resolution
		prodGallery.each(function () {
			var $gallery = $(this);
			toggleGrid($gallery);
			$gallery.slick('slickGoTo', 0);
		});
	});

	// resize watcher, if screen width changed without resize event
	var screenWidth = $(window).innerWidth();
	setInterval(function () {
		var newScreenWidth = $(window).innerWidth();
		if (screenWidth !== newScreenWidth) {
			// toggle grid class depend of screen resolution
			prodGallery.each(function () {
				var $gallery = $(this);
				toggleGrid($gallery);
				$gallery.slick('slickGoTo', 0);
			});
			screenWidth = newScreenWidth;
		}
	}, 1000);

}); // ready