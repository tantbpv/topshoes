$(function() {
	// full_height
	function full_height() {
		var $page = $(".js-full-height");
		$page.css("min-height", $(window).height());
	}
		
	full_height();
	$(window).on("resize", full_height);
 	// full_height end
});