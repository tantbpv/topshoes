$(document).ready(function () {

	$("img.lazy").lazyload({
		effect: "fadeIn",
		threshold: 500,
		skip_invisible: true
	});

	// select
	var select = $(".js-select");
	select.styler({
		onSelectOpened: function() {
			//console.log("onSelectOpened");
      $(".js-custom-scrollbar").mCustomScrollbar("update");
    },
		onSelectClosed: function() {
			//console.log("onSelectClosed");
			// wait while select will close 
      setTimeout( function() {
				$(".js-custom-scrollbar").mCustomScrollbar("update");
			}, 10);
    }
	});
	var searchSelect = $(".js-select-seach");
	searchSelect.styler({
		selectSearch: true,
		selectSearchLimit: 5,
		selectSearchPlaceholder: "Найти..."
	});
	// select end
	

	
	// custom scrollbar in cart
	$(window).load(function () {
		$(".js-custom-scrollbar").mCustomScrollbar({
			setHeight: "100%",
			callbacks: {
				onUpdate: function() {
					//console.log("scrollbar onUpdate");
				}
			}
		});
	});
	$(window).on("resize", function() {
		$(".js-custom-scrollbar").mCustomScrollbar("update");
	});
	
	
	
}); // ready



