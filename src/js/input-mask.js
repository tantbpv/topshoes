$(document).ready(function () {

	// input phone mask
	try {
		$('.js-phone-mask').attr('autocomplete', 'off').attr('placeholder', '+38 (0__) ___-__-__').inputmask({
      "mask": "+38 (099) 999-99-99",
      "placeholder": "_",
    });
	} catch (e) {
		console.log(e);
	}
	// input mask end

}); // ready