$(function () {
	
	var $pagination = $(".js-pagination"),
	    $paginationList = $(".js-pagination").find(".pagination__list"),
			$activeLink = $pagination.find(".active"),
			$body = $("body"),
			activeClass = "is-open";

	$activeLink.on("click", function (e) {
		e.preventDefault();
		e.stopPropagation();
		if($(this).hasClass(activeClass)) {
			hideMenu.apply($(this));
		} else {
			hideMenu.apply($activeLink);
			showMenu.apply($(this));
		}
	});

	function hideMenu () {
		$(this).removeClass(activeClass);
	}

	function showMenu () {
		$(this).addClass(activeClass);
	}

	$body.on("click", function (e) {
		if(e.target.class !== activeClass) {
			hideMenu.apply($activeLink);
		}
	});

	$paginationList.on('click', function (e) {
		e.stopPropagation();
	});

	$(window).resize(function() {
		hideMenu.apply($activeLink);
	});

}); //ready