jQuery(function() {
	// add google analytic events
	// registration page
	$("#user-register-form").on("submit", function() {
		ga('send', 'event', 'registration', 'submit', "registrationpage");
	});

	// product page
	$(".js-ga-callback-click").on("click", function() {
		ga('send', 'event', 'fast-order', 'click');
	});

	$(".js-ga-callback-submit").on("submit", function() {
		ga('send', 'event', 'fast-order', 'submit');
	});

	// add to cart
	$(".js-ga-to-cart-btn").on("click", function() {
		ga('send', 'event', 'add-to-cart', 'click', "all");
	});

	//checkout
	$(".js-ga-checkout-click").on("click", function() {
		ga('send', 'event', 'checkout', 'click');
	});

	$(".js-checkout-form").on("submit", function() {
		ga('send', 'event', 'checkout', 'submit');
	});
});



