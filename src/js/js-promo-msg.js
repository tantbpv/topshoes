(function() {
	var btn = $(".js-promo-msg"),
			state = "is-visible";

	function hideMsg (elem) {
		elem.hide().removeClass(state);
	}

	function showMsg (elem) {
		elem.show().addClass(state);
	}

	btn.on('click', function(event) {
		event.preventDefault();

		var msgContetn = $(this).siblings('.form__promo-message');

		if(msgContetn.hasClass(state)) {
			hideMsg(msgContetn);
		} else {
			showMsg(msgContetn);
		}

	});

	btn.hover(function(event) {
		var msgContetn = $(this).siblings('.form__promo-message');
		if (event.type == "mouseenter") {
        showMsg(msgContetn);
    }
    else { // mouseleave
        hideMsg(msgContetn);
    }
	});

}());