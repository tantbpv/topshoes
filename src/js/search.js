$(function () {
	var $autocompleteInput = $(".js-autocomplete");

	$autocompleteInput.each(function (i, el) {
		var $searchInstance = $(el);
		var $searchForm = $searchInstance.parents("form");
		var $searchResult = $searchInstance.parent().find(".js-search-result");

		// init jQuery UI autocomplete plugin
		$searchInstance.autocomplete({
			appendTo: $searchResult,
			classes: {
				"ui-autocomplete": "search_list"
			},
			position: {of: $searchResult},
			minLength: 2,
			delay: 1000,
			source: function (request, response) {
				// get json with search results
				$.ajax({
					url: $searchForm.attr("action"),
					dataType: "json",
					data: request.term,
					success: function (data) {
						response(data.results);
					}
				});
			},
			focus: function (event, ui) {
				// set input value = focus item name
				// $searchInstance.val(ui.item.name);
				return false;
			},
			select: function (event, ui) {
				// redirect to product page
				window.location.href = ui.item.url;
				//alert("go to " + ui.item.url);
			}
		});

		// search result item template
		$searchInstance.autocomplete("instance")._renderItem = function (ul, item) {
			return $("<li>")
					.append("<div class='search_item'><img class='search_img' src='" + item.image + "' alt='" + item.name + "'><div class='search_info'><p class='search_name'>" + item.name + "</p>" + "<p class='search_category'>" + item.category + "</p>" + "<p class='search_price'>" + item.price + "</p></div></div>")
					.appendTo(ul);
		};

	});// end .each()
});// ready
