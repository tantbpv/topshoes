// sticky-block.js 
$(function () {


	function checkPosition() {
		var $block = $(".js-stick-review"),
			$track = $(".js-stick-review-track"),
			$menu = $(".js-fix-menu"),
			media = window.matchMedia("only screen and (max-width: 62em)").matches,
			vScroll = $(window).scrollTop() + $menu.outerHeight() + 50,
			topEdge = $track.offset().top,
			bottomEdge = topEdge + $track.height() - $block.height(),
			rightEdge = $track.offset().left + $track.outerWidth(),
			windowWidth = $(window).outerWidth();
		//console.log("rightEdge " + rightEdge);
		//console.log("windowWidth " + windowWidth);


		function setPosition() {
			if (topEdge > vScroll) {
				//console.log("top");
				$block.attr("style", "")
					.removeClass("fixed-bottom")
					.removeClass("fixed");
			} else if (topEdge <= vScroll && bottomEdge > vScroll) {
				//console.log("middle");
				$block.addClass("fixed")
					.removeClass("fixed-bottom");

				$block.css({
					'position': 'fixed',
					'top': $menu.height() + 50,
					'bottom': "auto",
					'right': windowWidth - rightEdge
				});

			} else if (bottomEdge <= vScroll) {
				//console.log("bottom"); 
				$block.css({
					'position': 'absolute',
					'top': 'auto',
					'bottom': 0,
					'right': 0
				});
				$block.addClass("fixed-bottom");
			} else {
				console.error("Stiky block position error");
			}
		}
		if (!media) {
			setPosition();
		} else {
			$block.attr("style", "")
				.removeClass("fixed-bottom")
				.removeClass("fixed");
		}
	}
	// init 
	try {
		checkPosition();
		$(window).on("scroll", checkPosition);
		$(window).on("resize", checkPosition);
	} catch (e) {
		//console.info("Sticky block not found");
	}
}); // ready
// sticky-block.js  end