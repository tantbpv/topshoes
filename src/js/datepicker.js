// datepicker
pickmeup.defaults.locales['ru'] = {
	days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
	daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
	daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
	months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
	monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
};

try {
	pickmeup('.js-date', {
		format  : 'd/m/Y',
		default_date: false,
		calendars: 1,
		hide_on_select: true,
		locale: "ru"
	});
} catch(e) {
	//console.log(e);
}


try {
	var maxDate = new Date();

	pickmeup('.js-birth-date', {
		format  : 'd/m/Y',
		default_date: false,
		calendars: 1,
		hide_on_select: true,
		locale: "ru",
		render: function (date) {
			if (date > maxDate) {
				return {
					disabled: true,
					class_name: 'is-disabled'
				};
			}
			return {};
		}
	});
} catch(e) {
	//console.log(e);
}
// datepicker end