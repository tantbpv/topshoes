(function ($) {
	// prived methods

	var closeClass = "is-close";
	var speed = 200;
	var isClose = false;

	var openAccordion = function (settings) {
		$(this).removeClass(closeClass);
		$(this).find(".accordion_content").slideDown(speed);

		if (settings.open) {
			settings.open.call(this);
		}
	};

	var closeAccordion = function (settings) {
		$(this).addClass(closeClass);
		$(this).find(".accordion_content").slideUp(speed);

		if (settings.close) {
			settings.close.call(this);
		}
	};

	var closeAll = function (settings) {
		$accrodions = $(this).parents().find(".accordion");
		$accrodions.each(function (i, el) {
			closeAccordion.call(el, settings);
		});
	};
	// prived methods end

	// public methods
	var methods = {
		init: function (options) {
			// default option
			var settings = $.extend({
				init: function () {},
				open: function () {},
				close: function () {},
			}, options);

			return this.each(function () {
				var $accrodion = $(this).find(".accordion");
				var data = $(this).data("simpleAccordion");

				// if plugin was not init
				if (!data) {
					// init plugin
					console.log(settings);
					if($accrodion.length) {
						$accrodion.each(function (i, el) {
							var btn = $(el).find(".accordion_title");
							closeAccordion.call(el, settings);

							btn.on("click", function () {
								if($(el).hasClass(closeClass)) {
									closeAll.call(el, settings);
									openAccordion.call(el, settings);
								} else {
									closeAccordion.call(el, settings);
								}
							});
						});// end .each()
					}

					if (settings.init) {
						settings.init.call(this);
					}

					$(this).data("simpleAccordion", true);
				}
			});
		}
	};
	// public methods end
	$.fn.simpleAccordion = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Methods' + method + ' does not exist for jQuery.simpleAccordion');
		}
	};

})(jQuery);

jQuery(function ($) {
	$(".js-accordions").simpleAccordion({
		init: function () {
			//console.log(this);
		},
		open: function () {
			//console.log("open");
		},
		close: function () {
			//console.log("close");
		},
	});
});
