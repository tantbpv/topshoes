// sliders.js
var arrowLeft = '<i class="fa fa-angle-left" aria-hidden="true"></i>';
var arrowRight = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
$(document).ready(function () {
	prodSliderInit();
	fvSliderInit();
}); // ready


var prodSliderInit = function () {
	// product page slider
	var prodSlider = $('.js-product-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		dots: true,
		arrows: true,
		nextArrow: '<button type="button" class="slider__btn--next slider__btn"> ' + arrowRight + ' </button>',
		prevArrow: '<button type="button" class="slider__btn--prev slider__btn">' + arrowLeft + '</button>',
		infinite: true,
		lazyLoad: 'ondemand',
		swipeToSlide: true,
		accessibility: false,
		adaptiveHeight: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {}
			}
		]
	});

	function createThumbs(images) {
		//console.log(images);
		var $thumbsWrapper = $(".js-thumb-slider");

		images.forEach(function (src, index) {
			var $thumbTemplate = $('<div class="col-xs-6"><a href="#" data-slide-index="' + index + '" class="product_thumb"><img src="' + src + '" alt="slide ' + index + '"></a></div>');
			$thumbsWrapper.append($thumbTemplate);
		});
		// custom pagination for product slider

		function setCurrentSlideMarker(index) {
			var $pagItems = $thumbsWrapper.find(".product_thumb");
			var $currPagItem = $thumbsWrapper.find(".product_thumb[data-slide-index=" + index + "]");
			//console.log("current slide " + index);

			$pagItems.removeClass("active");
			$currPagItem.addClass("active");
		}

		// listen pagination click events
		$thumbsWrapper.find(".product_thumb").on("click", function (e) {
			var slideIndex = $(this).attr("data-slide-index");

			e.preventDefault();
			setCurrentSlideMarker(slideIndex);
			prodSlider.slick('slickGoTo', slideIndex);
		});

		// listen slider slide events
		prodSlider.on('afterChange', function (slick, currentSlide, index) {
			setCurrentSlideMarker(index);
		});
		setCurrentSlideMarker(0);
	}

	function getImgSrc(slider) {
		var slidesLength = slider.slick('getSlick').$slides.length;
		var images = [];

		for (var i = 0; i < slidesLength; i++) {
			var imgUrl = $(slider.slick('getSlick').$slides[i]).find(" > img ").attr("src");
			images.push(imgUrl);
		}
		return images;
	}

	// init only when slider exist
	try {
		createThumbs(getImgSrc(prodSlider));
		//console.info("prodSlider init");
	} catch (err) {
		//console.info("prodSlider does not exist");
		//console.error(err);
	}

	$(window).resize(function () {
		prodSlider.slick('slickGoTo', 0);
	});
	prodSlider.slick('slickGoTo', 0);

	// product page slider end
};

var fvSliderInit = function () {
	// product page slider
	var fvSlider = $('.js-fv-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		dots: false,
		arrows: false,
		infinite: false,
		lazyLoad: 'ondemand',
		swipe: false,
		swipeToSlide: false,
		accessibility: false,
		//asNavFor: ".js-fv-thumbs",
		responsive: [
			{
				breakpoint: 992,
				settings: {
					swipe: true
				}
			}
		]
	});


	// init new thumbs slider
	var $fvThumbsslider = $(".js-fv-thumbs").slick({
		slidesToShow: 12,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		infinite: false,
		lazyLoad: 'ondemand',
		swipeToSlide: true,
		accessibility: false,
		focusOnSelect: true,
		asNavFor: ".js-fv-slider"
	});


	$(window).resize(function () {
		fvSlider.slick('slickGoTo', 0);
	});
	fvSlider.slick('slickGoTo', 0);

	// product page slider end
};
// sliders.js end