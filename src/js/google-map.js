// google map api
//AIzaSyAIYGiIg16yEH1P5_ApbBTFJzKhGtM4dT4
//
var gMap;
var markersInstance = {};

function closeInfoWindow() {
	if (infoWindow) {
		infoWindow.close();
		// for (var markerKey in markersInstance) {
		// 	markersInstance[markerKey].setIcon("img/icon/marker.png");
		// }
	}
}

function  customizeInfoWindow(infoWindow) {
	// *
	// START INFOWINDOW CUSTOMIZE.
	// The google.maps.event.addListener() event expects
	// the creation of the infowindow HTML structure 'domready'
	// and before the opening of the infowindow, defined styles are applied.
	// *
	if (infoWindow) {

		google.maps.event.addListener(infoWindow, 'domready', function () {

			// Reference to the DIV that wraps the bottom of infowindow
			var iwOuter = $('.gm-style-iw');

			/* Since this div is in a position prior to .gm-div style-iw.
			 * We use jQuery and create a iwBackground variable,
			 * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
			 */
			var iwBackground = iwOuter.prev();

			// Removes background shadow DIV
			iwBackground.children(':nth-child(2)').css({'display': 'none'});

			// Removes white background DIV
			iwBackground.children(':nth-child(4)').css({'display': 'none'});

			// set infoWindow width.
			/*iwOuter.parent().css({
				width: '260px'
			});*/

			// Moves the infowindow 115px to the right.
			iwOuter.parent().parent().css({left: '0px'});

			// Moves the shadow of the bottom arrow.
			iwBackground.children(':nth-child(1)').css('display', "none");
			// Remove the bottom arrow.

			iwBackground.children(':nth-child(3)').css('display', "none");

			// Changes the desired tail shadow color.
			/*iwBackground.children(':nth-child(3)').find('div').children().css({
			 'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px',
			 'z-index': '1'
			 });*/

			// Reference to the div that groups the close button elements.
			var iwCloseBtn = iwOuter.next();
			// hide close btn
			iwCloseBtn.css('display', "none");
			// Apply the desired effect to the close button
			// iwCloseBtn.css({
			// 	opacity: '1',
			// 	right: '10px',
			// 	top: '10px',
			// 	border: 'none'
			// });

			// If the content of infowindow not exceed the set maximum height, then the gradient is removed.
			/*if ($('.iw-content').height() < 140) {
			 $('.iw-bottom-gradient').css({display: 'none'});
			 }*/

			// The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
			iwCloseBtn.mouseout(function () {
				$(this).css({opacity: '1'});
			});
		});
	}
}

var infoWindow = null;
function addInfoWindow(marker, message) {

	// show infoWindow
	google.maps.event.addListener(marker, 'click', function () {
		closeInfoWindow();

		infoWindow = new google.maps.InfoWindow({
			content: '<div class="map-info"><div class="map-info__content">' + message + '</div></div>',
			maxWidth: 260
		});
		// set custom styles
		customizeInfoWindow(infoWindow);
		// hide marker
		//marker.setIcon("img/icon/marker-empty.png");

		// waiting styles
		setTimeout(function () {
			infoWindow.open(gMap, marker);
		}, 70);

	});

}//addInfoWindow

function initMap(mapConfig) {

	// Specify features and elements to define styles.
	var styleArray = [
		{
			featureType: "all",
			stylers: [
				{
					saturation: 0
				}
			]
		}, {
			featureType: "poi.business",
			//elementType: "labels",
			stylers: [
				{
					visibility: "off"
				}
			]
		}
	];

	var isDraggable = $(document).width() > 768 ? true : false; // If document (your website) is wider than 768px, isDraggable = true, else isDraggable = false

	var mapOptions = {
		//draggable: isDraggable,
		center: {
			lat: mapConfig.mapCenter[0],
			lng: mapConfig.mapCenter[1]
		},
		// Apply the map style array to the map.
		styles: styleArray,
		zoom: mapConfig.zoom,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		panControl: false,
		zoomControl: true,
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: true,
		overviewMapControl: false,
		scrollwheel: false // Prevent users to start zooming the map when scrolling down the page
		//... options options options
	};
	// Create a map object and specify the DOM element for display.
	gMap = new google.maps.Map(document.getElementById('g-map'), mapOptions);

	// Event that closes the Info Window with a click on the map
	google.maps.event.addListener(gMap, 'click', function () {
		closeInfoWindow();
	});

	// create markers
	var markers = mapConfig.markers;
	var markerPosition = {};

	for (var marker in markers) {

		// // create new marker position instance
		markerPosition[marker] = new google.maps.LatLng(markers[marker].location[0], markers[marker].location[1]);
		// create new marker instance
		markersInstance[marker] = new google.maps.Marker({
			position: markerPosition[marker],
			map: gMap,
			title: markers[marker].title,
			icon: mapConfig.markersImg
		});

		// create and show infoWindow on click
		addInfoWindow(markersInstance[marker], markers[marker].baloonContent);


		//console.log(markersInstance[marker]);
	}


}// initMap

function centeringMap(location) {
	var latitude = location[0],
			longitude = location[1],
			zoom = location[2] || 16;
	//centering map
	gMap.setZoom(zoom);
	gMap.panTo({lat: latitude, lng: longitude});

}

$(window).on("load", function () {
	if (typeof mapConfig != "undefined") {
		initMap(mapConfig);
	}
});
// google map api end