var PopUp = (function ($) {
	// private methods
	var _removalDelay = 100;
	var _scrollBarWidth = Detect.scrollBarWidth;

	var isOpenPopup = function () {
		// console.log("open pop-up");
		$('body').css('padding-right', _scrollBarWidth + "px");
		$("html").addClass("pop-up-is-showed");
	};

	var isClosePopup = function () {
		// console.log("close pop-up");
		$("html").removeClass("pop-up-is-showed");
		$('body').css('padding-right', 0);
	};


	return {
		// public methods

		open: function (html) {
			$.magnificPopup.close();
			//wait for animation
			setTimeout(function () {
				// Open directly via API
				$.magnificPopup.open({
					items: {
						src: html, // can be a HTML string, jQuery object, or CSS selector
						type: 'inline'
					},
					removalDelay: _removalDelay,
					mainClass: 'mfp-fade',
					overflowY: 'scroll',
					//showCloseBtn: false,
					callbacks: {
						open: function () {
							isOpenPopup();
							Tabs.init(".js-tabs");
						},
						close: isClosePopup
					}
				});
			}, _removalDelay);
		},
		openAjax: function (url, method) {
			var _this = this;
			$.ajax({
				url: url,
				type: method,
				success: function (data) {
					//console.log(data);
					_this.open(data);
				}
			});

		},
		close: function () {
			$.magnificPopup.close();
		}
	}
}(jQuery));

jQuery(function ($) {

}); // ready



// pop-up.js

function isOpenPopup() {
	//console.log('pop-up is open');
	$('body').css('padding-right', Detect.scrollBarWidth + "px");
	$("html").addClass("pop-up-is-showed");
}

function isClosePopup() {
	//console.log('pop-up is close');
	$("html").removeClass("pop-up-is-showed");
	$('body').css('padding-right', 0);
}

function initPopUps() {
	$(".js-pop-up").data("ignore-scroll", true);
	$(".js-pop-up").magnificPopup({
		type: 'inline',
		preloader: false,
		removalDelay: 100,
		mainClass: 'mfp-fade',
		//fixedContentPos: false,
		overflowY: 'scroll',
		callbacks: {
			open: isOpenPopup,
			close: isClosePopup
		}
	});
}

$(document).ready(function () {
	initPopUps();
	$(".js-pop-up-ajax").data("ignore-scroll", true);
	$(".js-pop-up-ajax").magnificPopup({
		type: 'ajax',
		preloader: true,
		focus: false,
		//showCloseBtn: false,
		removalDelay: 100,
		mainClass: 'mfp-fade',
		//fixedContentPos: false,
		overflowY: 'scroll',
		callbacks: {
			open: isOpenPopup,
			ajaxContentAdded: function () {
				setTimeout(fvSliderInit, 50);
				//fvSliderInit();
				initAlert();
				initPopUps();
				Tabs.init(".js-tabs");
			},
			close: isClosePopup
		}
	});
}); // ready
// pop-up.js end