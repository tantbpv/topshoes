$(function () {
	// filter script start 
	var filter = $(".js-filter");
	var filterItem = filter.find(".filter__item");
	var filterDropdown = filter.find(".filter__dropdown");
	var filterform = $(".js-filter-form");
	var filterSubmit = $(".js-filter-submit");
	var filterClear = $(".js-filter-remove");
	var filterData = filter.find("input");
	var dropedClass = "filter__item--is-droped";
	var hasChange = false;
	//  active filter label class
	var activeClass = "filter__item--is-active";

	function submitFillter() {
		hasChange = false;
		filterItem.each(setLabel);
		var $form = filterform;
		/*
		 // sending form via ajax
		 $.ajax({
		 url: $form.attr('action') + "?ajax=true",
		 type: $form.attr('method') || "POST",
		 data: $form.serialize(),
		 success: function (response) {
		 hasChange = false;
		 console.log("fillter submit: " + response)
		 },
		 error: function () {
		 alert('Возникла ошибка, нет связи с сервером. Попробуйте еще раз.');
		 console.log('error!');
		 }
		 });
		 */
	}

	function showFilterDropdown() {
		$(this).addClass(dropedClass);
	}

	function hideFilterDropdown() {
		filterItem.removeClass(dropedClass);
	}

	// set rigth position for dropdown filter
	function setFilterPosition() {
		var DrDw = $(this).find(".filter__dropdown");
		var viewportWidth = $(window).outerWidth();
		var DrDwPosition = DrDw.offset().left;
		var DrDwWidth = DrDw.outerWidth();
		if (DrDwPosition + DrDwWidth > viewportWidth) {
			DrDw.css({
				"left": "auto",
				"right": "0",
				"-webkit-transform": "translateX(0) translateY(0)",
				"transform": "translateX(0) translateY(0)"
			});
		}
		if (DrDwPosition < 0) {
			DrDw.css({
				"left": "0",
				"right": "auto",
				"-webkit-transform": "translateX(0) translateY(0)",
				"transform": "translateX(0) translateY(0)"
			});
		}
	}

	function isOpen() {
		if (filterItem.hasClass(dropedClass)) {
			return true;
		} else {
			return false;
		}
	}

	// is filter change
	filterData.on("change", function (e) {
		hasChange = true;
	});

	function setData() {
		var filterText = [];
		var $filter = $(this);
		var $filterInputs = $filter.find("input");
		var type = null;
		// get data.type and data.text
		$filterInputs.each(function () {
			if ($(this).attr("type") === "checkbox" && this.checked) {
				filterText.push($(this).next("span").html());
				type = "checkbox";
			}
			if ($(this).attr("type") === "radio" && this.checked) {
				filterText.push($(this).next("span").html());
				type = "radio";
			}
			if ($(this).attr("type") === "text") {
				if ($(this).attr("id") === "amount-min") {
					filterText[0] = $(this).val();
				}
				if ($(this).attr("id") === "amount-max") {
					filterText[1] = $(this).val();
				}
				type = "text";
			}
		});

		var $minInput = $filter.find('#amount-min');
		var $maxInput = $filter.find('#amount-max');
		var minRange = parseInt($minInput.data('default'));
		var maxRange = parseInt($maxInput.data('default'));
		// if price have a default value
		if (parseInt(filterText[0]) === minRange && parseInt(filterText[1]) === maxRange) {
			filterText = [];
		}

		// set data
		$filter.data("filterData", {
			text: filterText,
			type: type
		});
		//console.log($filter.data("filterData"));
	}

	// data flow:
	// > input > data > label
	// set data > label 
	function setLabel() {
		var $filter = $(this);
		var $filterLabel = $filter.find(".filter__lable > i");
		var $filterInputs = $filter.find("input");
		var filterText = $filter.data("filterData").text;
		var filterType = $filter.data("filterData").type;
		// set/unset active status for filter
		if (filterText.length) {
			$filter.addClass(activeClass);
		} else {
			$filter.removeClass(activeClass);
		}

		// set/unset chosen filters
		if (filterType === "radio" || filterType === "checkbox") {
			if (filterText.length === 1) {
				$filterLabel.html("" + filterText);
			} else if (filterText.length > 1) {
				$filterLabel.html("" + filterText.length);
			} else if (filterText.length === 0) {
				$filterLabel.html("");
			}
		}
		// for price filter
		if (filterType === "text") {
			if (filterText.length) {
				$filterLabel.html("" + filterText[0] + " - " + filterText[1] + " грн");
			} else {
				$filterLabel.html("");
			}
		}
	}

	function highlightBtn() {
		var $filter_item = $(this);
		var highlightClass = "is-highlight";
		var filterText = $filter_item.data("filterData").text;
		// set/unset highlight status for filter item
		if (filterText.length) {
			$filter_item.addClass(highlightClass);
		} else {
			$filter_item.removeClass(highlightClass);
		}
	}

	// get URL and transform to object
	// function getURL() {
	// 	var search = location.search.substring(1);
	// 	var searchObject = search ? JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
	// 		function (key, value) {
	// 			return key === "" ? value : decodeURIComponent(value);
	// 		}) : {};
	// 	return searchObject;
	// }

	// get input val and transform to URL
	// function setURL() {
	// 	var searchObject = getInput().join("&");
	// 	var URL = "?" + encodeURI(searchObject);
	// 	window.history.pushState(searchObject, "filter data", URL);
	// }

	// get URL and transform to object 
	// function setInput() {
	// 	var searchObject = getURL();
	// 	for (var key in searchObject) {
	// 		var name = key;
	// 		if (name === "price") {
	// 			continue;
	// 		}
	// 		if (name === "sort") {
	// 			filterItem.find("input[name='" + name + "'][value='" + searchObject[key].split(",")[0] + "']").attr("checked", true);
	// 			continue;
	// 		}
	// 		for (var i = 0; i < searchObject[key].split(",").length; i++) {
	// 			var value = searchObject[key].split(",")[i];
	// 			filterItem.find("input[name='" + name + "'][value='" + value + "']").attr("checked", true);
	// 		}
	// 	}
	// 	filterItem.find("input").trigger("change");
	// 	filterItem.find("input").on("change", function () {
	// 	});
	// }


	function clearInput() {
		var $filter = $(this);
		var $filterLabel = $filter.find(".filter__lable > i");
		var $inputs = $filter.find("input");
		$inputs.each(function () {
			if ($(this).attr("type") === "radio" || $(this).attr("type") === "checkbox") {
				$(this).attr("checked", false);
			} else if ($(this).hasClass("filter-slider__input")) {
				$(this).val($(this).attr("data-default"));
				$(this).trigger("change");
			} else {
				throw "Clear filter is fail.";
			}
		});
		$filterLabel.html("");
		$filter.removeClass(activeClass);
		$filter.removeClass(dropedClass);
	}

	function getInput() {
		var searchObject = [];
		filterItem.each(function () {
			var name = $(this).find("input").eq(0).attr("name");
			var value = [];
			var type = $(this).data("filterData").type;
			var text = $(this).data("filterData").text;
			if (type === "radio" || type === "checkbox") {
				$(this).find("input:checked").each(function () {
					value.push($(this).val());
				});
			}
			if (type === "text") {
				$(this).find("input").each(function () {
					value.push($(this).val());
				});
			}
			if (value.length) {
				searchObject.push(name + "=" + value.join(","));
			}
		});
		return searchObject;
	}


	// filter events

	// on page loaded
	filterItem.each(function () {
		//setInput.apply(this);
		setData.apply(this);
		setLabel.apply(this);
		highlightBtn.apply(this);
	});

	// set z-index to filter items
	for (var zIndex = filterItem.length + 1, i = 0; i < filterItem.length; i++, zIndex--) {
		filterItem.eq(i).css("z-index", zIndex);
	}

	// filter input changes
	filterData.on("change", function () {
		var $filter = $(this).parents(".filter__item");
		//getInput();
		//setURL();
		setData.apply($filter);
		highlightBtn.apply($filter);
		//setLabel.apply(this);
	});

	// clear filter btn
	filterClear.on("click", function (e) {
		var $filter = $(this).parents(".filter__item");
		e.stopPropagation();
		clearInput.apply($filter);
		//setURL();
		setData.apply($filter);
		setLabel.apply($filter);
		highlightBtn.apply($filter);
		// submit filter
		submitFillter();
	});

	var $clearAll = $(".js-clear-all-filter").on("click", function (e) {
		filterItem.each(function () {
			clearInput.apply($(this));
			//setURL();
			setData.apply($(this));
			setLabel.apply($(this));
			highlightBtn.apply(this);
		});
		if (hasChange) {
			submitFillter();
		}
		hideFilterDropdown();
	});


	// submit filter btn 
	filterSubmit.on("click", function (e) {
		e.preventDefault();
		if (hasChange) {
			submitFillter();
		}
		hideFilterDropdown();
	});


	// click for filter label
	filterItem.on("click", function (e) {
		e.stopPropagation();
		if ($(this).hasClass(dropedClass)) {
			if (hasChange) {
				submitFillter();
			}
			hideFilterDropdown();
		} else {
			hideFilterDropdown();
			showFilterDropdown.bind($(this))();
			setFilterPosition.bind($(this))();
		}
	});

	$("body").on("click", function (e) {
		if (e.target.className !== "filter__dropdown" || e.target.className !== "filterItem") {
			if (!isOpen()) {
				return;
			} else {
				if (hasChange) {
					submitFillter();
				}
				hideFilterDropdown();
			}
		}
	});

	filterDropdown.on("click", function (e) {
		e.stopPropagation();
	});

	$(window).on("resize", function () {
		// set initial state
		filterItem.removeClass(dropedClass);
		filterDropdown.attr("style", "");
		if (hasChange) {
			submitFillter();
		}
	});


	////////////////////
	// mobile filters
	var filterTitle = $(".filter_title");
	var filterList = $(".filter__item-list");

	function showAllFilters() {
		filterList.slideDown(500);
		filterTitle.addClass(dropedClass);
	}

	function hideAllFilters() {
		filterList.slideUp(500);
		filterTitle.removeClass(dropedClass);
	}

	filterTitle.on("click", function () {
		if ($(this).hasClass(dropedClass)) {
			hideAllFilters();
		} else {
			showAllFilters();
		}
	});

	$(window).on("resize", function () {
		var mobileMedia = window.matchMedia("only screen and (max-width: 47.94em)").matches;
		if (mobileMedia) {
			//hideAllFilters();
		} else {
			//showAllFilters();
			filterTitle.removeClass(dropedClass);
			filterList.attr("style", "");
		}
	});
	// mobile filters end 

	//filter script end 	
}); //ready