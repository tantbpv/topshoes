$(document).ready(function(){

	var $select = $(".js-dinamic-select"),
			$changeSelectInput = $(".js-select-change-show"),
			$inputToggle =	$changeSelectInput.children('label').find('input').parent('label'),
			$selectToggle =	$changeSelectInput.children('label').find('select').parent('label');

	function showInput () {
		$inputToggle.show();
	}

	function hideInput () {
		if($inputToggle.css("display") === "block") {
			$inputToggle.hide();
		}
	}

	function showSelect() {
		$selectToggle.show();
	}

	function hideSelect () {
		if($selectToggle.css("display") === "block") {
			$selectToggle.hide();
		}
	}

	hideSelect();

	$select.change(function(event) {
		var value = $(this).val();
   	if(value === "1") {
   		showSelect();
   		hideInput();
   	} else {
   		showInput();
   		hideSelect();
   	}
	});

}); // ready