"use strict";

var $locationTownBtns = $(".js-location-nav button");
var $locationTownSelect = $(".js-location-select");

function highlightTown(town) {
	$locationTownBtns.removeClass("is-current");
	// set location button state
	$locationTownBtns.filter("[data-town="+ town +"]").addClass("is-current");
	// set location select state
	$locationTownSelect.val(town).trigger('refresh');
}
$(function () {
	// init location pop-up btns
	$locationTownBtns.on("click", function () {
		highlightTown($(this).data("town"));
	});

	// init location pop-up select
	$locationTownSelect.on("change", function () {
		highlightTown($(this).val());
		// get location from data attr
		var location = eval( $(this).find("option:selected").data("location") );
		if (location && Array.isArray(location)) {
			centeringMap(location);
		}

	});
});//ready