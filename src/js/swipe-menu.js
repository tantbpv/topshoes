$(function () {
	// swipe menu
	var $touch = $('.js-toggle-menu');
	var $touch_animate = $('.js-toggle-menu .sandwich');
	var className = "active";
	var $menu = $('.swipe-menu');
	var $close = $('.js-menu-close');
	// pan event distance
	var deltaX = 0;

	function showMenu() {
		$("body").addClass(className);
		$touch_animate.addClass(className);
		// ios scroll fix
		$("html").addClass("prevent-ios-scroll");
		//console.log("show");
	}

	function hideMenu() {
		// reset touch events
		$menu.removeAttr("style");
		deltaX = 0;
		// hide menu
		$("body").removeClass(className);
		$touch_animate.removeClass(className);
		// ios scroll fix
		$("html").removeClass("prevent-ios-scroll");
	}

	$touch.on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();
		if (!($touch_animate.hasClass(className))) {
			showMenu();
		} else {
			hideMenu();
		}
	});
	$close.on('click', function (e) {
		e.stopPropagation();
		hideMenu();
	});
	$("body").on('click', function (e) {
		if (e.target.className !== "swipe-menu") {
			//hideMenu();
		}
	});
	$menu.on('click', function (e) {
		e.stopPropagation();
	});
	$(window).resize(function () {
		var wid = $(window).width();
		if (wid > 767) {
			hideMenu();
		}
	});
	// swipe menu end

	// touch events
	var swipeWrapper = document.getElementsByTagName('body');

	// enable  select text
	if ($(window).width() > 767) {
		delete Hammer.defaults.cssProps.userSelect;
	}


	// create a simple instance
	var swipeWrapperHammer = new Hammer(swipeWrapper[0], {
		// recognizers: [
		// 	[Hammer.Pan, {direction: Hammer.DIRECTION_HORIZONTAL}]
		// ]
	});

	// fix android scroll bug
	// var isScrolling = false;
	// $(window).on('scroll', function() {
	// 	isScrolling = true;
	// }, { passive: true });
	//
	// $(window).on('touchend', function() {
	// 	isScrolling = false;
	// });

	// listen to events...
	swipeWrapperHammer.on("swipeleft", function (e) {
		hideMenu();
	});

	/* swipeWrapperHammer.on("panleft panright", function (e) {
		//console.log(e);
		//console.log(e.angle);
		//if (isScrolling) return;

		if (!( (e.angle <= -160 || e.angle >= 160) || (e.angle >= -20 && e.angle <= 20) )) return;

		if (!($("body").hasClass(className))) return;
		//console.log(deltaX);
		//console.log("pan");
		deltaX = e.deltaX;
		if (deltaX >= -100 && deltaX < 0) {
			$menu.css({
				transform: "translateX(" + (deltaX) + "px)"
			});
		} else if (deltaX < -100) {
			hideMenu();
		}
	});
	swipeWrapperHammer.on("panend pancancel", function (e) {
		//console.log(e.type);
		// reset touch events
		$menu.removeAttr("style");
		deltaX = 0;
	});*/


});

$(function ($) {
	var $otherMenu = $(".js-other-menu");

	$('#dl-menu').dlmenu({
		animationClasses: {classin: 'dl-animate-in-2', classout: 'dl-animate-out-2'},
		onLevelClick: function () {
			$otherMenu.fadeOut(0);
			setTimeout(function () {
				$otherMenu.fadeIn(200);
			}, 300);
		}
	});
});