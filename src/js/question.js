var Question = (function ($) {

	var _check = function () {
		return true;
	};
	var _hideError = function (container) {
		container.removeClass("show-invalid");
	};
	var _showError = function (container) {
		console.log("validation error");
		container.addClass("show-invalid");
	};

	var getRadio = function(container) {
		var names = [];
		$(container).find("input[type='radio']").each(function () {
			var _name = $(this).attr('name');
			if (names.indexOf(_name) === -1) {
				names.push(_name);
			}
		});
		return names;
	};

	var validateRadio = function(name) {
		var radios = document.getElementsByName(name);
		var radioValid = false;
		var i = 0;
		while (!radioValid && i < radios.length) {
			if (radios[i].checked) radioValid = true;
			i++;
		}
		return radioValid;
	};

	function _checkForm(container) {

		var radios = getRadio(container);
		var rating = $(container).find("input.rating");

		//console.log(radios);
		//console.log(rating);
		// validate radio
		for (var i = 0; i < radios.length; i++) {
			if ( !validateRadio(radios[i]) ) {
				return false;
			}
		}
		// validate rating
		for (var j = 0; j < rating.length; j++) {
			if ( !rating.eq(j).val() || rating.eq(j).val() === 0 ) {
				return false;
			}
		}
		return true;
	}


	return {

		next: function (next) {
			// var container = $(this).parents(".pop-up");
			// if ( _checkForm(container) ) {
			// 	_hideError(container);
				PopUp.open(next);
			// } else {
			// 	_showError(container);
			// }
		},

		prev: function (prev) {
			PopUp.open(prev);
		},
		submit: function () {
			// var container = $(this).parents(".pop-up");
			// if ( _checkForm(container) ) {
			// 	_hideError(container);
				submitQuestionnaire();
			// } else {
			// 	_showError(container);
			// }
		}
	}
}(jQuery));