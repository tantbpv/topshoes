jQuery(function ($) {
	var $radio = $('.js-filter-form :radio');
	$radio.each(function () {
		if($(this).prop('checked')) {
			$(this).data('checked', true);
		} else {
			$(this).data('checked', false);
		}
	});

	$radio.on("click", function(){
		var isChecked = $(this).data('checked');
		var $radioGroup = $radio.filter(':radio[name=' + $(this).attr("name")+']');
		$radioGroup.each(function () {
			$(this).data('checked', false);
		});
		if(isChecked) {
			$(this).prop('checked', false);
			$(this).data('checked', false);
		} else {
			$(this).prop('checked', true);
			$(this).data('checked', true);
		}
	});
});