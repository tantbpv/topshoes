$(document).ready(function () {
	var headerHeight,
			$menu = $(".js-fix-menu"),
			$header = $(".js-f-m-header"),
			$topBanners = $(".js-f-m-top-banners");


	function setTopMenu(top) {
		var scrollTop = $(window).scrollTop();
		if (scrollTop <= top) {
			$menu.removeClass("is-fixed").css({
				position: "absolute",
				top: top
			});
		} else {
			$menu.addClass("is-fixed").css({
				position: "fixed",
				top: 0
			});
		}
	}

	function getHeaderHeight() {
		headerHeight = $header.outerHeight() + $topBanners.outerHeight();
		setTopMenu(headerHeight);
		//console.log(headerHeight);
	}

	getHeaderHeight();

	$(window).on("load", function () {
		getHeaderHeight();
		$menu.css({
			opacity: 1
		});
	});

	$(window).on("resize", function () {
		getHeaderHeight();
	});

	$(window).on("scroll", function () {
		getHeaderHeight();
	});

	function preventScroll() {
		if ($menu.hasClass("is-fixed") && $menu.hasClass("is-open")) {
			$('.js-fix-menu, body').css('padding-right', getScrollBarWidth() + "px");
			$("html, body").addClass("prevent-scroll");
		} else {
			$("html, body").removeClass("prevent-scroll");
			$('.js-fix-menu, body').css('padding-right', 0);
		}
	}

	// show submenu
	function showSubmenu() {
		$menu.addClass("is-open");
		$(this).addClass("is-active");
		preventScroll();
	}

	function hideSubmenu() {
		$menu.removeClass("is-open");
		$(this).removeClass("is-active");
		$("html, body").removeClass("prevent-scroll");
		$('.js-fix-menu, body').css('padding-right', 0);
	}

	$menu.find(" .top-menu__list > li ").hover(showSubmenu, hideSubmenu);

	$(window).on("scroll", preventScroll);
}); // ready